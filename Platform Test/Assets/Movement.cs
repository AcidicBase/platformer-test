﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed;
    public float velx;
    private Rigidbody rb;


	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float hor = Input.GetAxisRaw("Horizontal");

        Vector3 movement = new Vector3(hor, 0, 0);


        if (hor == 0)  
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
        //resets velocity of the rigidbody when button is released. It slides before stopping if I don't do this. 
        //It'd be better and more realistic if it's just really in10s deceleration


        if (Input.GetKeyDown(KeyCode.W)) //This shit is jump
        {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        }


        //velocity limiter

        if (rb.velocity.x > 5)
        {
            rb.velocity = new Vector3(5, rb.velocity.y, 0);
        }
        else if (rb.velocity.x < -5)
        {
            rb.velocity = new Vector3(-5, rb.velocity.y, 0);
        }
     
        rb.AddForce((movement * speed), ForceMode.VelocityChange);
        //velx = rb.velocity.x; //this is for troubleshooting


	}
}
